import React from "react";
import ProfileComponent from "../Components/ProfileComponent";
import styles from '../ModuleCss/ViewStyling.module.css'

const ProfilePage = () => {

  return (
    <div className={styles.paddingDiv}>
      <ProfileComponent />
    </div>
  );
};
export default ProfilePage;
