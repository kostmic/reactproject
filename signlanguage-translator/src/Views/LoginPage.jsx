import React from "react";
import LoginInput from "../Components/LoginComponent";
import styles from '../ModuleCss/ViewStyling.module.css'

const LoginPage = () => {
  return (
    <div className={styles.paddingDiv}>
      <LoginInput />
    </div>
  );
};
export default LoginPage;
