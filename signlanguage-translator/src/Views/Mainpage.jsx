import MainpageComponent from '../Components/MainpageComponent';
import styles from '../ModuleCss/ViewStyling.module.css'

const MainPage = () => {
  return (
    <div className={styles.paddingDiv}>
      <MainpageComponent />
    </div>
  );
};
export default MainPage;
