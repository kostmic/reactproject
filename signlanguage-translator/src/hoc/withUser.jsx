import { useUserContext } from "../Context/UserContext";
import { Redirect } from "react-router-dom";

const withUser = Component => props => {
    const { username } = useUserContext()
    if (username !== '') {
        return <Component {...props} />
    } else {
        return <Redirect to="/" />
    }
}

export default withUser
