const contentBox = (props) => {

    return(
        <div className="contentBox">
            {props.children}
            <div className="bottomStyle"></div>
        </div>
    )
}

export default contentBox