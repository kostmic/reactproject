import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import LoginPage from "./Views/LoginPage";
import MainPage from "./Views/Mainpage";
import ProfilePage from "./Views/ProfilePage";
import Header from "./Components/HeaderComponent";

function App() {
  return (
    <Router>
      <div>
        <Header />
        <Switch>
          <Route exact path="/" component={LoginPage} />
          <Route path="/translate" component={MainPage} />
          <Route path="/profile" component={ProfilePage} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
