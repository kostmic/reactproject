
import React, { createContext, useContext, useReducer } from "react";

const UserContext = createContext(null)

export const useUserContext = () => {
    return useContext(UserContext)
}

const userReducer = (state, action) => {
    switch (action.type) {
        case 'FETCH_USER':
            return {
                user: action.payload,
                isFetched: true
            }
            case 'POST_USER':
                return {
                    user: action.payload,
                    isPosted: true
                }
        default:
            return state
    }
}

const initialState = {
    user: {},
    isFetched: false,
    isPosted: false
}

const UserProvider = ({ children }) => {

    const [userState, dispatch] = useReducer(userReducer, initialState)

    return (
        <UserContext.Provider value={{ userState, dispatch }}>
            {children}
        </UserContext.Provider>
    )
}

export default UserProvider
