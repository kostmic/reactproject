import { useUserContext } from '../Context/UserContext'
import styles from '../ModuleCss/Header.module.css'
import { useHistory } from "react-router-dom";


const Header = () => {

    const { userState } = useUserContext()
    const history = useHistory()
    const RouteProfile = () => {
        history.push('/profile')
    }

    return (
        <nav className={styles.Header}>
            <img src="../../Logo.png" className={styles.Logo} alt="The website mascot"></img>
            <h1 className={styles.HeaderText}>Lost in translation</h1>
            {userState.user &&
                <div onClick={RouteProfile} className={styles.HeaderProfile}>
                    <p className={styles.ProfilePictureName}>{userState.user.username}</p>
                    <img src="../../ProfilePicture.png" className={styles.ProfilePicture} alt="this is a person"></img>
                </div>
            }
        </nav>
    )
}

export default Header
