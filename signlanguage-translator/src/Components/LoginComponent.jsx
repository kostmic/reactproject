import styles from '../ModuleCss/Input.module.css'
import { useHistory } from "react-router-dom";
import { useEffect, useState } from 'react';
import { useUserContext } from '../Context/UserContext'
import { apiURL, apiKey } from '../api/userAPI';

const LoginInput = () => {

    const { userState, dispatch } = useUserContext()
    const [username, setUsername] = useState('')
    const history = useHistory()

    const fetchUser = async () => {

        const response = await fetch(`${apiURL}/?username=${username}`)
        const user = await response.json()
        dispatch({ type: "FETCH_USER", payload: user[0] })
    }

    const postUser = async () => {
        const response = await fetch(`${apiURL}`, {
            method: 'POST',
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                translations: []
            })
            
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Could not create new user')
            }
            return response
        })

        const user = await response.json()
        dispatch({ type: "POST_USER", payload: user })
    }




     useEffect(() => {
        if(userState.user === undefined && !userState.isPosted){
            postUser()
        }else if( userState.user.username){
            history.push('/translate')
        }
    }, [userState, history]) 



    const handleLoginClick = () => {
         fetchUser()
    }

    const handleOnChange = event => {
        setUsername(event.target.value)
    }

    return (
        <div className={styles.InputContainer}>
            <div className={styles.InputComponent} id={styles.InputComponentLogin}>
                <img className={styles.KeyboardImage} src="../../../KeyboadSymbol.png" alt="" />
                <input className={styles.InputComponent.InputField} placeholder="Enter Username" onChange={handleOnChange}></input>
                <button onClick={handleLoginClick} className={styles.Button}><img className={styles.ArrowImage} src="../../../ArrowSymbol.png" alt="" /></button>
            </div>
        </div>
    )
}

export default LoginInput