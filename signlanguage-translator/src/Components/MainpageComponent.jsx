import styles from '../ModuleCss/Input.module.css'
import boxStyles from '../ModuleCss/ContentBox.module.css'
import { useState } from 'react';
import { useUserContext } from '../Context/UserContext'
import TranslationComponent from './TranslationComponent';

const MainpageComponent = (props) => {

    const { userState } = useUserContext()
    const [inputString, setInputString] = useState('')
    const [inputStringClicked, setInputStringClicked] = useState('')



    const UpdateUserTranslations = async () => {
        const apiURL = 'https://noroff-assignments-api.herokuapp.com/translations'
        const apiKey = 'd2ee8003-d36a-49cd-a4ed-b605101900eb'
        const userId = userState.user.id // Update user with id 1

        fetch(`${apiURL}/${userId}`, {
            method: 'PATCH', // NB: Set method to PATCH
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                // Provide new translations to add to user with id 1
                translations: userState.user.translations
            })
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Could not update translations history')
                }
                return response.json()
            })
            .then(updatedUser => {
                // updatedUser is the user with the Patched data
            })
            .catch(error => {
            })
    }

    const handleTranslateClick = () => {
        setInputStringClicked(inputString)
        if (inputString !== "") {
            userState.user.translations.push(inputString)
            UpdateUserTranslations()
            console.log(userState.user)
        }
    }

    const handleOnChange = event => {
        setInputString(event.target.value)
    }

    return (
        <div>
            <div className={styles.InputComponent} id={styles.InputComponentMainpage}>
                <img className={styles.KeyboardImage} src="../../../KeyboadSymbol.png" alt="" />
                <input
                    placeholder="Translation"
                    onChange={handleOnChange}
                >
                </input>
                <button
                    onClick={handleTranslateClick}
                    className={styles.Button}>
                    <img
                        className={styles.ArrowImage}
                        src="../../../ArrowSymbol.png"
                        alt="" />
                </button>

            </div>
            <div className={boxStyles.ContentWrapper}>
                <div className={boxStyles.ContextBox}>
                    <TranslationComponent inputString={inputStringClicked} />
                </div>
                <div className={boxStyles.BottomBar}><button className={boxStyles.TranslateButton}>Translation</button></div>
            </div>
        </div>
    )
}

export default MainpageComponent