import { useUserContext } from '../Context/UserContext'
import withUser from "../hoc/withUser";
import { useHistory } from "react-router-dom";
import styles from '../ModuleCss/Profile.module.css'
import { apiURL, apiKey } from '../api/userAPI';

const ProfileComponent = () => {

    const { userState, dispatch } = useUserContext()
    const history = useHistory()

    const deleteTranslations = async () => {
        fetch(`${apiURL}/${userState.user.id}`, {
            method: 'PATCH', // NB: Set method to PATCH
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                // Provide new translations to add to user with id 1
                translations: []
            })
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Could not update translations history')
                }
                return response.json()
            })
            .then(updatedUser => {
                // updatedUser is the user with the Patched data
            })
            .catch(error => {
            })

    }

    const handleDelete = () => {
        deleteTranslations()
        dispatch({ type: "FETCH_USER", payload: { id: userState.user.id, username: userState.user.username, translations: [] } })
    }

    const handleLogout = () => {
        dispatch({ type: "FETCH_USER", payload: { id: 0, username: "", translations: [] } })
        history.push('/')
    }

    const handleReturn = () => {
        history.push('/translate')
    }

    console.log(userState.user)

    return (
        <>
            <div>
                <h1>{userState.user.username}'s latest translations:</h1>
                {userState.user.username &&
                    <ul className={styles.TranslationsList}>
                        {userState.user.translations.slice(0, 10).map(translations => <li key={translations}>{translations}</li>)}
                    </ul>
                }
                <button onClick={handleDelete} className={styles.Buttons} id={styles.DeleteButton}>Delete Translations</button>
                <button onClick={handleLogout} className={styles.Buttons} id={styles.LogoutButton}>Logout</button>
                <button onClick={handleReturn} className={styles.Buttons} id={styles.MainpageButton}>Return to main page</button>

            </div>
        </>
    )
}

export default withUser(ProfileComponent)