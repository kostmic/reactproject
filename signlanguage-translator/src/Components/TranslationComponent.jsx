import styles from '../ModuleCss/Translation.module.css'

const TranslationComponent = (props) => {

    const characters = props.inputString.split('')

    const imageSign = characters.map((inputChar) =>
        inputChar !== ' ' ?
            <img className={styles.TranslationElement} alt="Hand gesture" src={`../../../IndividualSigns/${inputChar}.png`
            }/> : <img />
    )


    return (
        <div className={styles.TranslationBox}>
            {imageSign}
        </div>
    )
}

export default TranslationComponent